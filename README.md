This is the skeleton for building Django apps. Using this you can start creating django apps in seconds, literally.

It has support for 
Piston - A mini-framework for Django for creating RESTful APIs.

South - Intelligent schema and data migrations for Django projects

Memcached - A high-performance, distributed memory object caching system, generic in nature, but originally intended for use in speeding up dynamic web applications by alleviating database load

Jinja2 - A full featured template engine for Python. 

Celery - Celery using RabbitMQ. Celery is an async task queue based on distributed message passing
